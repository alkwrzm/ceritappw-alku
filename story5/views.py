from django.shortcuts import render, redirect
from .models import Schedule
from . import forms
from django.http import HttpResponse

def schedule(request):
    schedules = Schedule.objects.all().order_by('date')
    return render(request, 'schedule.html', {'schedules': schedules})

def schedule_create(request):
    if request.method == 'POST':
        form = forms.ScheduleForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('story5:schedule')

    else:
        form = forms.ScheduleForm()
    return render(request, 'schedule_create.html', {'form': form})

def schedule_delete(request, id):
    if request.method == 'POST':
        Schedule.objects.filter(id=id).delete()
        return redirect('story5:schedule')
    else:
        return HttpResponse("GET/ not allowed")
