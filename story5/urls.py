from django.urls import path
from . import views

app_name = 'story5'

urlpatterns = [
    path('', views.schedule, name='schedule'),
    path('/create', views.schedule_create, name='schedule_create'),
    path('clear/<int:id>/', views.schedule_delete, name='schedule_delete'),
]
