from django.db import models
from datetime import datetime

# Create your models here.

CATEGORY_CHOICES = (
    ('kuliah','Kuliah'),
    ('compfest', 'Compfest'),
    ('bem', 'BEM'),
    ('others', 'Others'),
)


class Schedule(models.Model):

    activity = models.CharField(max_length=30)
    date = models.DateTimeField(default=datetime.now)
    time = models.TimeField()
    place = models.CharField(max_length=20)
    category = models.CharField(max_length=20, choices=CATEGORY_CHOICES)

    def __str__(self):
        return self.activity
