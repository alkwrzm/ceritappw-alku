from django.urls import path
from . import views

app_name = 'story4'

urlpatterns = [
    path('', views.index, name='index'),
    path('profile/', views.profile, name='profile'),
    path('abilities/', views.abilities, name='abilities'),
    path('experiences/', views.experiences, name='experiences'),
    path('photograph/', views.photograph, name='photograph'),
    path('contact/', views.contact, name='contact'),
]