from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request, 'index.html')

def experiences(request):
    return render(request, 'experiences.html')

def abilities(request):
    return render(request, 'abilities.html')

def photograph(request):
    return render(request, 'photograph.html')

def profile(request):
    return render(request, 'profile.html')

def contact(request):
    return render(request, 'contact.html')